# GetLibs

## A simple wrapper for `whereis` that allows for one-lining your includes

> ```bash
> ./getLibs.py --prefix '-I' glfw GLU GL vulkan
> 
> > -I/usr/include/GLFW -I/usr/include/vulkan -I/usr/share/vulkan
> 
> includeDirs=`python ./getLibs.py --prefix '-I' glfw3 GLU GL vulkan`
> g++ main.c -o output.o -lglfw -lGLU -lGL $includeDirs -std=c++11
> 
> > g++ main.c \
> >    -o output.o \
> >    -lglfw -lGLU -lGL 
> >    -I/usr/include/GLFW -I/usr/include/vulkan -I/usr/share/vulkan
> >    -std=c++11 \
> ```

Takes name(s) of librar(y)/(ies) and returns locations.

`--all` : will return all matches to the base name regardless of the ending number, if present.

`--case` : will make the results case sensitive

`--prefix` : the following argument will be used to prefix each result

`--suffix` : the following argument will be used to suffix each result

Examples:

> ```bash
> ./getLibs.py
> ./getLibs.py --help
> ```

- will output this help screen again.


> ```bash
> ./getLibs.py --all sdl2 glfw3
> ```

- would output results for sdl, sdl2, glfw, and glfw3

> ```bash
> ./getLibs.py sdl2 glfw3
> ```
	
- would only output results for sdl2 and glfw3
- would default to sdl, if no sdl2 is found
- would default to glfw, if no glfw3 is found

> ```bash
> ./getLibs.py --case SDL2 glfw3
> ```

- would only output results for SDL2 and glfw3
- not for sdl, sdl2, GLFW, GLFW3
- would default to SDL and glfw, if neither numbered versions were found

> ```bash
> ./getLibs.py --prefix 'anything_' glfw vulkan
> 
> > anything_/usr/include/GLFW anything_/usr/include/vulkan
> ```


> ```bash
> ./getLibs.py --suffix '_END' glfw vulkan
> 
> > /usr/include/GLFW_END /user/include/vulkan_END
> ```

The suffix comes in handy when desiring to wrap individual results in quotes or parens
> ```bash
> ./getLibs.py --prefix '"' --sufffix '"' glfw vulkan
> 
> > "/usr/include/GLFW" "/usr/include/vulkan" 
> ```

