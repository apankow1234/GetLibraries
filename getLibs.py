#! /usr/bin/python2.7
import os, sys, subprocess, re

def all_casings(input_string):
	# https://stackoverflow.com/questions/6792803/finding-all-possible-case-permutations-in-python
	if not input_string:
		yield ""
	else:
		first = input_string[:1]
		if first.lower() == first.upper():
			for sub_casing in all_casings(input_string[1:]):
				yield first + sub_casing
		else:
			for sub_casing in all_casings(input_string[1:]):
				yield first.lower() + sub_casing
				yield first.upper() + sub_casing

class GetLibs :
	
	def __init__( self, request ) :
		if not re.search( '^[^0-9]+', request ) :
			exit(0)
		# public :
		self.query = request
		self.base = re.match( '^[^0-9]+', request ).group(0)
		self.end = request[len(self.base):]
		self.prefix = ""
		self.suffix = ""
		# private :
		self.__searchVariations = []
		self.__libs = []
		self.__priorityFind = 1
		self.__allFind = 0
		self.__caseSensitive = 0
		self.__getVariations = 1

	def ToggleFindings( self ) :
		self.__priorityFind = ( self.__priorityFind + 1 ) % 2
		self.__allFind = ( self.__allFind + 1 ) % 2
		return self

	def ToggleCaseSensitivity( self ) :
		self.__caseSensitive = ( self.__caseSensitive + 1 ) % 2
		self.__getVariations = ( self.__getVariations + 1 ) % 2
		return self
	
	def SetPrefix( self, prefix="" ) :
		self.prefix = str( prefix )
	
	def SetSuffix( self, suffix="" ) :
		self.suffix = str( suffix )

	def GetLibs( self ) :
		self.__GetSearchVariations()
		self.__FindLibs()
		return [ self.prefix + x + self.suffix for x in self.__libs ]

	def __GetSearchVariations( self ) :
		allOpts = list()
		searchOptions = [ self.base ]
		if self.__getVariations and not self.__caseSensitive :
			searchOptions = [ x for x in all_casings( self.base ) ]
		for opt in searchOptions :
			allOpts.append( str(opt) )
			allOpts.append( str(opt) + str(self.end) )
		self.__searchVariations = allOpts
		return self

	def __FindLibs( self ) :
		findings = []
		for opt in self.__searchVariations :
			found = re.split( '[\:]', str(subprocess.check_output([ 'whereis', opt ])) )
			if found[1].strip() :
				for opt in re.split( '\s+', found[1].strip() ) :
					if opt not in findings :
						findings.append( opt )
		if self.__priorityFind and not self.__allFind :
			if any( [ True if self.end in x else False for x in findings ] ) :
				for finding in findings :
					if self.end in finding :
						self.__libs.append( finding )
				return self
		self.__libs = findings
		return self
	

def printHelp() :
	output = ["GetLibs v. 1.0.0" ]
	output.append( "By: Andrew Pankow" )
	output.append( "" )
	output.append( "Takes name(s) of librar(y)/(ies) and returns locations." )
	output.append( "" )
	output.append( "--all : will return all matches to the base name regardless of the ending number, if present." )
	output.append( "" )
	output.append( "--case : will make the results case sensitive" )
	output.append( "" )
	output.append( "--prefix : the following argument will be used to prefix each result" )
	output.append( "" )
	output.append( "--suffix : the following argument will be used to suffix each result" )
	output.append( "" )
	output.append( "Examples:" )
	output.append( "\t`" + __file__ + "`" )
	output.append( "\t`" + __file__ + " --help`" )
	output.append( "\t\t- will output this help screen again." )
	output.append( "" )
	output.append( "\t`" + __file__ + " --all sdl2 glfw3`" )
	output.append( "\t\t- would output results for sdl, sdl2, glfw, and glfw3" )
	output.append( "" )
	output.append( "\t`" + __file__ + " sdl2 glfw3`" )
	output.append( "\t\t- would only output results for sdl2 and glfw3" )
	output.append( "\t\t- would default to sdl, if no sdl2 is found" )
	output.append( "\t\t- would default to glfw, if no glfw3 is found" )
	output.append( "" )
	output.append( "\t`" + __file__ + " --case SDL2 glfw3`" )
	output.append( "\t\t- would only output results for SDL2 and glfw3" )
	output.append( "\t\t- not for sdl, sdl2, GLFW, GLFW3" )
	output.append( "\t\t- would default to SDL and glfw, if neither numbered versions were found" )
	output.append( "" )
	output.append( "\t`" + __file__ + " --prefix 'anything_' glfw vulkan`" )
	output.append( "\t\t- anything_/usr/include/GLFW anything_/usr/include/vulkan" )
	output.append( "" )
	output.append( "\t`" + __file__ + " --suffix '_END' glfw vulkan`")
	output.append( "\t\t- /usr/include/GLFW_END /user/include/vulkan_END" )
	output.append( "" )
	output.append( "\t`" + __file__ + " --prefix '\"' --sufffix '\"' glfw vulkan` ")
	output.append( "\t\t- \"/usr/include/GLFW\" \"/usr/include/vulkan\" " )
	output.append( "\n" )
	print "\n".join( output )


def run() :
	allLibs = list()
	toggleFinds = False
	toggleCase = False
	prefix = ""
	suffix = ""
	if len( sys.argv ) is 1 :
		printHelp()
		return
	elif "--help" in sys.argv :
		sys.argv.remove("--help")
		printHelp()
		return
	else :
		if "--all" in sys.argv :
			sys.argv.remove("--all")
			toggleFinds = True
		if "--case" in sys.argv :
			sys.argv.remove("--case")
			toggleCase = True
		if "--prefix" in sys.argv :
			prefixIdx = sys.argv.index( "--prefix" )
			nextVal = sys.argv[prefixIdx + 1]
			del( sys.argv[prefixIdx] )
			del( sys.argv[prefixIdx] )
			prefix = nextVal if nextVal else ""
		if "--suffix" in sys.argv :
			suffixIdx = sys.argv.index( "--suffix" )
			nextVal = sys.argv[suffixIdx + 1]
			del( sys.argv[suffixIdx] )
			del( sys.argv[suffixIdx] )
			suffix = nextVal if nextVal else ""

	for arg in sys.argv[1:] :
		thisSet = GetLibs( arg )
		if toggleFinds :
			thisSet.ToggleFindings()
		if toggleCase :
			thisSet.ToggleCaseSensitivity()
		thisSet.SetPrefix( prefix )
		thisSet.SetSuffix( suffix )
		allLibs += thisSet.GetLibs()
	print " ".join( allLibs )

run()
